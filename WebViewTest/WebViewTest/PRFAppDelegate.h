//
//  PRFAppDelegate.h
//  WebViewTest
//
//  Created by Christian Sánchez on 8/3/12.
//  Copyright (c) 2012 christian.sanchez@fairplaylabs.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRFViewController;

@interface PRFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) PRFViewController *viewController;

@end
