//
//  PRFViewController.m
//  WebViewTest
//
//  Created by Christian Sánchez on 8/3/12.
//  Copyright (c) 2012 christian.sanchez@fairplaylabs.com. All rights reserved.
//

#import "PRFViewController.h"

@interface PRFViewController ()

@end

@implementation PRFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
