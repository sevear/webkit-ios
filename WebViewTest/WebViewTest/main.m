//
//  main.m
//  WebViewTest
//
//  Created by Christian Sánchez on 8/3/12.
//  Copyright (c) 2012 christian.sanchez@fairplaylabs.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PRFAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PRFAppDelegate class]));
    }
}
